Module to search files on a local folder and send to a Apache Solr integrated with
Apache Tika, to extract metadata and content from the files and index them.

You can use Solr 3.5.0 or 3.6.x.

Copy solrconfig.xml from this module to APACHE_SOLR/conf/solrconfig.xml
Copy schema.xml from this module to the APACHE_SOLR/conf/schema.xml

ps: it's useful to symlink those files, instead of copy

1 - Start Apache Solr
2 - Install module
3 - Run the drush command "drush solr-local"
4 - Access /files/search to search your data

P.S.: You may need an extra cache clear after enabling the module to get the custom
search page to get into the menu router