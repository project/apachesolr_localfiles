<?php

/**
 * Module settings form.
 */
function apachesolr_localfiles_settings_form($form, &$form_state) {
  $form['apachesolr_localfiles_folder'] = array(
    '#type' => 'textfield',
    '#title' => t('Which system folder should be indexed?'),
    '#description' => t('You need to specify the folder to be monitored for new files to be indexed.'),
    '#default_value' => variable_get('apachesolr_localfiles_folder', ''),
    '#required' => TRUE,
  );
  $file_extensions_to_be_searched = array(
    'doc',
    'docx',
    'odt',
    'pdf'
  );
  $form['apachesolr_localfiles_mimetypes_allowed'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Which filetypes should be indexed?'),
    '#description' => t('The types specified here will be used as filters in the search for files to be indexed.'),
    '#options' => drupal_map_assoc($file_extensions_to_be_searched),
    '#default_value' => variable_get('apachesolr_localfiles_mimetypes_allowed', ''),
    '#required' => TRUE,
  );
  return system_settings_form($form);
}